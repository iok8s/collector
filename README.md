# Collector

REST API that receives a POST request with a JSON containing a `data` and an `id` field and sends the content of it to a Kafka clúster.

For running the server:

```bash
go run cmd/collector/main.go  -topic topic_test -partition 0 -kafkaAddress localhost:9092 -deadline 5 -serverAddress :5000
```

For emulating i.e 4 devices for 10 seconds with a maximum timeout between messages of 20ms:

```bash
go run cmd/emulator/main.go -nDevices 4 -timeout 10 -maxTimeMsg 20
```

> **All parameters are optional in both programs.**

You can also build a Docker image with

```
docker build -t collector -f deployments/Dockerfile .
```

And use the parameters in the `docker run` command as follows:

```
docker run collector "-kafkaAddress=10.201.5.78:9093"
```

## File structure

![Repo schema](assets/images/schema.png)

## TLS certificates

Run:

```bash
openssl req  -new  -newkey rsa:2048 \
   -nodes  -keyout cmd/collector/localhost.key \
   -subj /CN=localhost/ \
   -out cmd/collector/localhost.csr
```

to generate the key and sign it with:
```bash
openssl  x509  -req  -days 365  -in cmd/collector/localhost.csr  -signkey cmd/collector/localhost.key  -out cmd/collector/localhost.crt
```

## Testing against a Kafka server

If you don't have a Kafka clúster just run one with docker-compose:
```bash
git clone https://github.com/wurstmeister/kafka-docker

# https://towardsdatascience.com/kafka-docker-python-408baf0e1088
cp deployments/docker-compose-expose.yml kafka-docker/docker-compose-expose.yml

docker-compose -f kafka-docker/docker-compose-expose.yml up -d
```

Then just run `go test ./... -benh=.` to execute all the tests in the repo.