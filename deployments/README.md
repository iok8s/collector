# Deployments

- `docker-compose-expose.yml` is for running a Kafka cluster in local.
- `docker-compose.yml` runs a local Kafka cluster and a golang image with the API that run `go test`.
- `Dockerfile` is for building an image with this code.