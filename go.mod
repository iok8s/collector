module gitlab.com/iok8s/collector

go 1.16

require (
	github.com/Szeliga/goray v0.0.0-20160902155041-955dece9799a
	github.com/c2h5oh/datasize v0.0.0-20200825124411-48ed595a09d2 // indirect
	github.com/influxdata/tdigest v0.0.1 // indirect
	github.com/mailru/easyjson v0.7.7 // indirect
	github.com/prometheus/client_golang v1.10.0 // indirect
	github.com/rogerwelin/cassowary v0.14.0 // indirect
	github.com/segmentio/kafka-go v0.4.13
	github.com/segmentio/ksuid v1.0.3
	github.com/sirupsen/logrus v1.8.1
	github.com/tsenart/go-tsz v0.0.0-20180814235614-0bd30b3df1c3 // indirect
	github.com/tsenart/vegeta v12.7.0+incompatible // indirect
	golang.org/x/net v0.0.0-20210520170846-37e1c6afe023 // indirect
)
