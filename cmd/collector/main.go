package main

import (
	"flag"
	"net/http"
	"os"
	"time"

	log "github.com/sirupsen/logrus"

	"gitlab.com/iok8s/collector/internal/server"
)

var certFile = getenv("CERT_FILE", "cmd/collector/localhost.crt")
var keyFile = getenv("KEY_FILE", "cmd/collector/localhost.key")

const defaultTopic = "topic_test"
const defaultPartition = 0
const defaultAddress = "localhost:9092"
const defaultDeadline = 5
const defaultServingAddress = ":5000"
const defaultRuleEngineUrl = "http://localhost:3000"

var topic string
var partition int
var address string
var intDeadline int
var servingAddress string
var ruleEngineUrl string

func main() {
	flag.StringVar(&topic, "topic", defaultTopic, "Kafka topic where posted data will be sent")
	flag.IntVar(&partition, "partition", defaultPartition, "Kafka topic partition where messages will be sent")
	flag.StringVar(&address, "kafkaAddress", defaultAddress, "Kafka cluster address")
	flag.IntVar(&intDeadline, "deadline", defaultDeadline, "deadline for publishing a message to kafka in seconds")
	flag.StringVar(&servingAddress, "serverAddress", defaultServingAddress, "address where the server will listen")

	flag.StringVar(&ruleEngineUrl, "ruleEngineUrl", defaultRuleEngineUrl, "address where the rule engine is listening")

	deadline := time.Duration(intDeadline) * time.Second

	flag.Parse()

	p, err := server.NewKafkaPublisher(address, topic, deadline, partition)
	if err != nil {
		log.Fatal("cannot initiate the kafka publisher")
	}

	re := server.RedisRuleEngine{Url: ruleEngineUrl}
	defer p.Destroy() // Close the connection when leaving main
	server := &server.CollectorServer{Pub: p, RuleEngine: &re}

	log.Infof("Serving at %q", servingAddress)
	log.Fatal(http.ListenAndServeTLS(servingAddress, certFile, keyFile, server))
}

func getenv(key, fallback string) string {
	value := os.Getenv(key)
	if len(value) == 0 {
		return fallback
	}
	return value
}
