package main

import (
	"crypto/tls"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"math/rand"
	"net/http"
	"sync"
	"sync/atomic"
	"time"

	"gitlab.com/iok8s/collector/internal/emulator"
	"gitlab.com/iok8s/collector/internal/integration"
)

const defaultNDevices = 1
const defaultTimeout = 10
const defaultMaxTimeBetweenMessages = 1
const defaultPostDataUrl = "https://localhost:5000/collect"

var nDevices int
var timeout int
var maxTimeBetweenMessages int
var dStack []emulator.Device
var postDataUrl string

func main() {
	flag.IntVar(&nDevices, "nDevices", defaultNDevices, "Number of devices to emulate")
	flag.IntVar(&timeout, "timeout", defaultTimeout, "Default timeout of the program")
	flag.IntVar(&maxTimeBetweenMessages, "maxTimeMsg", defaultMaxTimeBetweenMessages, "Maximum miliseconds between consecutive messages from the same device")
	flag.StringVar(&postDataUrl, "url", defaultPostDataUrl, "Collector URL")
	flag.Parse()

	loop_timeout := time.Duration(timeout) * time.Second

	log.Printf("Emulating %d devices", nDevices)

	for i := 1; i <= nDevices; i++ {
		dStack = append(dStack, emulator.NewGeneralDevice())
	}

	var wg sync.WaitGroup
	wg.Add(nDevices)

	var ops uint64

	// Ignore self signed certificates
	http.DefaultTransport.(*http.Transport).TLSClientConfig = &tls.Config{InsecureSkipVerify: true}

	for _, d := range dStack {
		go func(d emulator.Device) {
			defer wg.Done()
			for start := time.Now(); time.Since(start) < loop_timeout; {
				emulate(d)
				atomic.AddUint64(&ops, 1)
				time.Sleep(time.Duration(rand.Float32()) * time.Millisecond)
			}
		}(d)
	}
	wg.Wait()

	fmt.Println("Number of requests:", ops)
}

func emulate(d emulator.Device) {
	client := http.Client{}
	p, err := d.GeneratePayload(d.Measure())
	if err != nil {
		log.Fatalf("error generating Payload for device %v", d.Id)
	}

	req, err := integration.MakeRequest(postDataUrl, p)
	if err != nil {
		log.Fatalf("error generating the request, %v", err)
	}

	resp, err := client.Do(req)
	if err != nil {
		log.Fatalf("error sending request for device %v, %v", d.Id, err)
	}

	if resp == nil {
		log.Fatalf("couldn't get a response for device %v", d.Id)
	} else {
		if resp.StatusCode != http.StatusAccepted {
			defer resp.Body.Close()
			body, _ := ioutil.ReadAll(resp.Body)
			log.Printf("response is: %q", string(body))
			log.Fatalf("got status %v, want %v", resp.StatusCode, http.StatusAccepted)
		}
	}
}
