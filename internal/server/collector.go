package server

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"strings"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	log "github.com/sirupsen/logrus"
)

// Publisher implements a Publish function that returns an error and a Destoy function that closes the possible connections it may have
type Publisher interface {
	Publish(id string, payload interface{}) error
	Destroy() error
}

type RuleEngine interface {
	isBlocked(id string, payload []byte) (bool, error)
}

type CollectorServer struct {
	Pub        Publisher
	RuleEngine RuleEngine
}

type DataJSON map[string]json.RawMessage

const WrongMethodErrorMsg = "method not allowed"
const EmptyBodyMsg = "the request body was empty or unprocessable"
const CannotDecodeErrorMsg = "cannot decode request body into json"
const CannotFindDataErrorMsg = "cannot find a data field in the request body or is empty"
const CannotFindIdErrorMsg = "cannot find an id field in the request body or is empty"
const CannotPublishMessage = "cannot publish the message"

var ForbiddenMessage = http.StatusText(http.StatusForbidden)

func (c *CollectorServer) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	router := http.NewServeMux()
	router.Handle("/collect", http.HandlerFunc(c.dataHandler))
	router.Handle("/metrics", promhttp.Handler())

	router.ServeHTTP(w, r)
}

var (
	opsProcessed = promauto.NewCounter(prometheus.CounterOpts{
		Name: "collector_processed_ops_total",
		Help: "The total number of processed messages",
	})
	opsCurrent = promauto.NewGauge(prometheus.GaugeOpts{
		Name: "collector_current_ops",
		Help: "The current number of requests",
	})
	processingTime = prometheus.NewHistogram(prometheus.HistogramOpts{
		Name:    "collector_processing_time_seconds",
		Help:    "The processing time of messages in seconds.",
		Buckets: prometheus.LinearBuckets(0.005, 0.005, 10),
	})
)

func init() {
	prometheus.MustRegister(processingTime)
	log.SetLevel(log.DebugLevel)
}

func (c *CollectorServer) dataHandler(w http.ResponseWriter, r *http.Request) {
	timer := prometheus.NewTimer(processingTime)
	defer timer.ObserveDuration()

	opsProcessed.Inc()
	opsCurrent.Inc()
	defer opsCurrent.Dec()

	if r.Method != http.MethodPost {
		http.Error(w, WrongMethodErrorMsg, http.StatusMethodNotAllowed)
		return
	}

	errorMsg, statusCode, body := getBody(r)
	if errorMsg != "" {
		log.Error("There was an error getting the body,", errorMsg)
		http.Error(w, errorMsg, statusCode)
		return
	}

	errorMsg, statusCode, id, payload := parseBody(body)
	if errorMsg != "" {
		log.Error("There was an error parsing the body,", errorMsg)
		http.Error(w, errorMsg, statusCode)
		return
	}

	blocked, err := c.RuleEngine.isBlocked(id, body)
	if err != nil {
		log.Errorf("There was an error checking if id %s was blocked, %v", id, err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		return
	}
	if blocked {
		log.Debugf("Device %s is blocked", id)
		http.Error(w, ForbiddenMessage, http.StatusForbidden)
		return
	}

	err = c.Pub.Publish(id, payload)
	if err != nil {
		log.Error("There was an error publishing the message,", err)
		http.Error(w, CannotPublishMessage, http.StatusInternalServerError)
		return
	}
	w.WriteHeader(http.StatusAccepted)
}

func getBody(r *http.Request) (errorMsg string, statusCode int, body []byte) {
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		return EmptyBodyMsg, http.StatusUnprocessableEntity, nil
	}
	if string(body) == "" {
		return EmptyBodyMsg, http.StatusUnprocessableEntity, nil
	}
	return "", 0, body
}

func parseBody(body []byte) (errorMsg string, statusCode int, id string, payload json.RawMessage) {
	var dataJSON DataJSON
	err := json.Unmarshal(body, &dataJSON)
	if err != nil {
		return CannotDecodeErrorMsg, http.StatusBadRequest, "", nil
	}

	rawId := dataJSON["id"]
	if len(rawId) == 0 {
		return CannotFindIdErrorMsg, http.StatusBadRequest, "", nil
	}
	id = string(rawId)
	id = strings.Replace(id, "\"", "", -1)

	payload = dataJSON["data"]
	if len(payload) == 0 {
		return CannotFindDataErrorMsg, http.StatusBadRequest, "", nil
	}
	return "", 0, id, payload
}
