package server

import (
	"bytes"
	"fmt"
	"net/http"

	log "github.com/sirupsen/logrus"
)

type RedisRuleEngine struct {
	Url string
}

func (r *RedisRuleEngine) isBlocked(id string, payload []byte) (bool, error) {
	client := &http.Client{}
	req, err := http.NewRequest("GET", fmt.Sprintf("%s/auth/%s", r.Url, id), bytes.NewBuffer(payload))
	if err != nil {
		return false, err
	}
	resp, err := client.Do(req)
	if err != nil {
		log.Error(err)
		return true, err
	}
	if resp.StatusCode != http.StatusOK {
		return true, nil
	}
	return false, nil
}

type StubRuleEngine struct{}

func (s *StubRuleEngine) isBlocked(id string, payload []byte) (bool, error) {
	if id == "unauthorizedid" {
		return true, nil
	} else {
		return false, nil
	}
}
