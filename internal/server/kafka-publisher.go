package server

import (
	"context"
	"encoding/json"
	"strings"
	"time"

	log "github.com/sirupsen/logrus"

	kafka "github.com/segmentio/kafka-go"
)

type KafkaPublisher struct {
	conn   *kafka.Conn
	config ConnectionConfig
}

type ConnectionConfig struct {
	address   string
	topic     string
	deadline  time.Duration
	partition int
}

func NewKafkaPublisher(address, topic string, deadline time.Duration, partition int) (Publisher, error) {
	config := ConnectionConfig{
		address:   address,
		topic:     topic,
		deadline:  deadline,
		partition: partition,
	}

	conn, err := GetConnection(config)
	if err != nil {
		log.Error("failed to create a connector: ", err)
		return nil, err
	}
	return &KafkaPublisher{conn, config}, nil
}

func GetConnection(config ConnectionConfig) (*kafka.Conn, error) {
	conn, err := kafka.DialLeader(context.Background(), "tcp", config.address, config.topic, config.partition)
	if err != nil {
		log.Error("failed to dial kafka leader, check the address of the cluster: ", err)
		return nil, err
	}
	err = conn.SetWriteDeadline(time.Now().Add(config.deadline))
	if err != nil {
		log.Error("failed to set write deadline: ", err)
		return nil, err
	}
	return conn, nil
}

func (p *KafkaPublisher) Publish(id string, payload interface{}) error {
	message, err := p.encodePayload(id, payload)
	if err != nil {
		log.Error("failed to encode message: ", err)
		return err
	}
	_, err = p.conn.WriteMessages(message)
	if err != nil {
		if strings.Contains(err.Error(), "i/o timeout") {
			log.Warn("The connection has been timed out")
			err := p.RenewConnection()
			if err != nil {
				log.Error("Error renewing the connection: ", err)
				return err
			}
		} else if strings.Contains(err.Error(), "use of closed network connection") {
			log.Warn("The connection is still closed, waiting before rewriting")
			time.Sleep(10 * time.Millisecond)
		} else {
			log.Error("Couldn't write the message, ", err)
			return err
		}

		_, err = p.conn.WriteMessages(message)
		if err != nil {
			log.Error("Couldn't write the message at second attemp: ", err)
			return err
		}
	}

	log.Debug("Message written!")
	return nil
}

func (p *KafkaPublisher) RenewConnection() error {
	log.Info("Renewing")
	p.conn.Close()
	new_conn, err := GetConnection(p.config)
	p.conn = new_conn
	return err
}

func (p *KafkaPublisher) Destroy() error {
	log.Debug("Destroying the publisher")
	return p.conn.Close()
}

func (p *KafkaPublisher) encodePayload(id string, payload interface{}) (kafka.Message, error) {
	log.Debug("Encoding the payload")
	m, err := json.Marshal(payload)
	if err != nil {
		return kafka.Message{}, err
	}

	return kafka.Message{
		Key:   []byte(id),
		Value: m,
	}, nil
}
