package server

import (
	"fmt"
	"os"
	"testing"
	"time"
)

const topic = "topic_test"
const partition = 0

var address = fmt.Sprintf("%s:9092", getenv("KAFKA_HOST", "localhost"))

const deadline = 10 * time.Second

func TestKafkaPublisher(t *testing.T) {

	p, err := NewKafkaPublisher(address, topic, deadline, partition)

	if err != nil {
		t.Errorf("Cannot create a kafka publisher, %v", err)
	}

	id := "id_test"
	b := []byte("Hello World!")
	err = p.Publish(id, b)

	if err != nil {
		t.Error(err)
	}
}

func getenv(key, fallback string) string {
	value := os.Getenv(key)
	if len(value) == 0 {
		return fallback
	}
	return value
}
