package server

import (
	"bytes"
	"errors"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"gitlab.com/iok8s/collector/internal/integration"
)

var tests = []struct {
	name             string
	jsonStr          []byte
	expectedStatus   int
	expectedErrorMsg error
	publishError     error
}{
	{
		name:             "returns 202 if can parse the data field",
		jsonStr:          []byte(`{"data":{"data2": "too cold."}, "another_field": "too hot.", "id": "ok"}`),
		expectedStatus:   http.StatusAccepted,
		expectedErrorMsg: nil,
		publishError:     nil,
	},
	{
		name:             "returns 400 if cannot parse the data field",
		jsonStr:          []byte(`{"not_data":"too cold.", "id": "ok"}`),
		expectedStatus:   http.StatusBadRequest,
		expectedErrorMsg: errors.New(CannotFindDataErrorMsg),
		publishError:     nil,
	},
	{
		name:             "returns 400 if cannot parse the id field",
		jsonStr:          []byte(`{"not_data":"too cold", "not_id": "ok"}`),
		expectedStatus:   http.StatusBadRequest,
		expectedErrorMsg: errors.New(CannotFindIdErrorMsg),
		publishError:     nil,
	},
	{
		name:             "returns 400 if cannot decode a json",
		jsonStr:          []byte(`{"not data": `),
		expectedStatus:   http.StatusBadRequest,
		expectedErrorMsg: errors.New(CannotDecodeErrorMsg),
		publishError:     nil,
	},
	{
		name:             "returns 422 if body is empty",
		jsonStr:          []byte(``),
		expectedStatus:   http.StatusUnprocessableEntity,
		expectedErrorMsg: errors.New(EmptyBodyMsg),
		publishError:     nil,
	},
	{
		name:             "returns 500 if cannot publish",
		jsonStr:          []byte(`{"data": "ok", "id": "3"}`),
		expectedStatus:   http.StatusInternalServerError,
		expectedErrorMsg: errors.New(CannotPublishMessage),
		publishError:     errors.New("error"),
	},
	{
		name:             "returns 403 if unauthorized in the rule engine",
		jsonStr:          []byte(`{"data": "ok", "id": "unauthorizedid"}`),
		expectedStatus:   http.StatusForbidden,
		expectedErrorMsg: errors.New(ForbiddenMessage),
		publishError:     nil,
	},
}

func TestPOSTData(t *testing.T) {
	for _, tt := range tests {
		server := initServer(t, tt.publishError)
		t.Run(tt.name, func(t *testing.T) {
			request, err := integration.MakeRequest("/collect", tt.jsonStr)
			if err != nil {
				t.Errorf("got error while making the request, %v", err)
			}
			response := httptest.NewRecorder()

			server.ServeHTTP(response, request)

			assertStatus(t, response.Code, tt.expectedStatus)
			assertErrorMessage(t, response.Body.String(), tt.expectedErrorMsg)
		})
	}
}

func TestGETData(t *testing.T) {
	t.Run("cannot use other method than post i.e get", func(t *testing.T) {
		jsonStr := []byte(`{"data": "value"}`)
		request, _ := http.NewRequest(http.MethodGet, "/collect", bytes.NewBuffer(jsonStr))
		request.Header.Set("Content-Type", "application/json")
		response := httptest.NewRecorder()

		server := initServer(t, nil)
		server.ServeHTTP(response, request)

		assertStatus(t, response.Code, http.StatusMethodNotAllowed)
		assertErrorMessage(t, response.Body.String(), errors.New(WrongMethodErrorMsg))
	})
}

func assertStatus(t *testing.T, got, want int) {
	if got != want {
		t.Errorf("got status %v, want %v", got, want)
	}
}

func assertErrorMessage(t *testing.T, a string, b error) {
	a = strings.TrimSuffix(a, "\n") // http.Error adds a "\n" at the end
	if b == nil {                   // if the error is nil we cannot use error.Error()
		if a != "" {
			t.Errorf("got error message '%v', want ''", a)
		}
		return
	}

	if strings.Compare(a, b.Error()) != 0 {
		t.Errorf("got error message '%v', want '%v'", a, b)
	}
}

func initServer(t *testing.T, expectedErr error) *CollectorServer {
	s := integration.StubPublisher{
		Err: expectedErr,
	}
	re := StubRuleEngine{}
	server := &CollectorServer{&s, &re}
	return server
}
