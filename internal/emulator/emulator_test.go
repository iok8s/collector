package emulator

import (
	"math/rand"
	"testing"
)

var randomSeed = int64(4)

func init() {
	rand.Seed(randomSeed)
}

var device = NewGeneralDevice()

var fieldsToCheck = []string{"humidity", "temperature", "security camera"}

func TestDeviceMeasure(t *testing.T) {
	gotMeasure := device.Measure()

	for _, field := range fieldsToCheck {
		assertKeyAndValueInMap(t, gotMeasure, field)
	}
}

func TestDeviceGetPayload(t *testing.T) {
	measure := map[string]string{
		"humidity":        GenerateRandomInt(),
		"temperature":     GenerateRandomFloat(),
		"security camera": GenerateRandomImage(),
	}

	_, err := device.GeneratePayload(measure)
	if err != nil {
		t.Errorf("Error generating the payload, %v", err)
	}
}

func assertKeyAndValueInMap(t *testing.T, m map[string]string, key string) {
	value, exists := m[key]
	if !exists {
		t.Errorf("wanted %v as key in %v", key, m)
	}

	if value == "" {
		t.Errorf("value of %v is empty", key)
	}

}
