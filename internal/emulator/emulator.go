package emulator

import (
	"bytes"
	"encoding/base64"
	"encoding/json"
	"image"
	"image/color"
	"image/png"
	"math/rand"
	"strconv"
	"time"

	"github.com/Szeliga/goray/engine" // image creation
	"github.com/segmentio/ksuid"      // uuid generation
)

type Device struct {
	Id      string
	sources map[string]string
}

func NewGeneralDevice() Device {
	return Device{
		Id: ksuid.New().String(),
		sources: map[string]string{
			"humidity":        "int",
			"temperature":     "float",
			"security camera": "image",
		},
	}
}

const randomImageWidth = 200
const randomImageHeight = 200
const floatPrecission = 4

type result struct {
	fieldName  string
	fieldValue string
}

func (d *Device) Measure() map[string]string {
	measure := make(map[string]string)
	measureChannel := make(chan result)

	for sensor, dataType := range d.sources {
		go func(s, d string) {
			measureChannel <- result{s, getRNGFromType(d)()}
		}(sensor, dataType)
	}

	for i := 0; i < len(d.sources); i++ {
		m := <-measureChannel
		measure[m.fieldName] = m.fieldValue
	}

	return measure
}

func (d *Device) GeneratePayload(measure map[string]string) ([]byte, error) {
	payloadAsMap := map[string]interface{}{
		"id":   d.Id,
		"data": measure,
	}
	return json.Marshal(payloadAsMap)
}

func getRNGFromType(dataType string) func() string {
	switch dataType {
	case "int":
		return GenerateRandomInt
	case "float":
		return GenerateRandomFloat
	case "image":
		return GenerateRandomImage
	}
	return nil
}

func GenerateRandomInt() string {
	n := rand.Intn(100)
	return strconv.Itoa(n)
}

func GenerateRandomFloat() string {
	f := rand.Float64() * 100
	return strconv.FormatFloat(f, 'f', floatPrecission, 64)
}

func GenerateRandomImage() string {
	scene := engine.NewScene(randomImageWidth, randomImageHeight)
	scene.EachPixel(func(x, y int) color.RGBA { return randomColor(x, y) })

	imgBase64Str := imageToBase64(scene.Img)
	return "data:image/png;base64," + imgBase64Str
}

func randomColor(x, y int) color.RGBA {
	rand := rand.New(rand.NewSource(time.Now().Unix()))
	return color.RGBA{
		uint8(x * rand.Intn(255) / randomImageWidth),
		uint8(y * rand.Intn(255) / randomImageHeight),
		uint8(rand.Intn(255)),
		255}
}

func imageToBase64(img image.Image) string {
	var buf bytes.Buffer
	png.Encode(&buf, img)
	return base64.StdEncoding.EncodeToString(buf.Bytes())
}
