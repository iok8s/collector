package integration

import (
	"bytes"
	"net/http"
)

func MakeRequest(url string, payload []byte) (*http.Request, error) {
	request, err := http.NewRequest(http.MethodPost, url, bytes.NewBuffer(payload))
	if err != nil {
		return nil, err
	}
	request.Header.Set("Content-Type", "application/json")
	return request, nil
}

type StubPublisher struct {
	Err error
}

func (s *StubPublisher) Publish(id string, payload interface{}) error {
	return s.Err
}

func (s *StubPublisher) Destroy() error {
	return nil
}
