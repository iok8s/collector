package integration

import (
	"net/http"
	"net/http/httptest"
	"testing"

	"gitlab.com/iok8s/collector/internal/emulator"
	"gitlab.com/iok8s/collector/internal/server"
)

func BenchmarkMakeRequest(b *testing.B) {
	d := emulator.NewGeneralDevice()
	publisher := StubPublisher{
		Err: nil,
	}

	re := server.StubRuleEngine{}
	server := &server.CollectorServer{Pub: &publisher, RuleEngine: &re}

	for i := 0; i < b.N; i++ {
		m := d.Measure()
		p, err := d.GeneratePayload(m)
		if err != nil {
			b.Errorf("error generating payload, %v", err)
		}
		req, err := MakeRequest("/collect", p)
		if err != nil {
			b.Errorf("error generating the request %v", err)
		}
		response := httptest.NewRecorder()

		server.ServeHTTP(response, req)
		if response.Code != http.StatusAccepted {
			b.Errorf("got status %v, want %v", response.Code, http.StatusAccepted)
		}
	}
}
